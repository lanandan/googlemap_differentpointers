package com.example.gr_map_example;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends Activity {

	// Google Map
	private GoogleMap googleMap;
	TextView edt;
	int toggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		edt=(TextView)findViewById(R.id.timings);
		
		edt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toggleclick();
			}
		});
		
		try {
			// Loading map
			initilizeMap();
			
			// Changing map type
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			 //googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

			/*double latitude = 17.385044;
			double longitude = 78.486671;*/
			edt.setText("James Jacob"+" - In time 10.00 A.M"+"\n"+"Jacob Ethan"+" - In time 11.10 A.M"
			+"\n"+"Daniel Anthony"+" - In time 11.10 A.M"+"\n"+"William Alexander"+" - In time 02.10 P.M"
					+"\n"+"Michael Ryan"+" - In time 04.00 P.M"+"\n"+"Hagrid"+" - In time 05.20 P.M"
					+"\n"+"Mike"+" - Location Cannot be determined"+"\n"+"Richard"+" - Location Cannot be determined");       
			// lets place some 10 random markers
			MarkerOptions marker;
			for (int i = 0; i < 10; i++) {
				// random latitude and logitude
				/*double[] randomLocation = createRandLocation(latitude,
						longitude);*/

				// Adding a marker
				/*MarkerOptions marker = new MarkerOptions().position(
						new LatLng(randomLocation[0], randomLocation[1]))
						.title("Hello Maps " + i);*/

				/*Log.e("Random", "> " + randomLocation[0] + ", "
						+ randomLocation[1]);*/

				marker = new MarkerOptions();
				
				
				// changing marker color
				if (i == 0)
					marker.position(new LatLng(38.907098,-77.036455)).title("James Jacob")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
				//			.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				if (i == 1)
					marker.position(new LatLng(38.907983,-77.034094));
					//marker.icon(BitmapDescriptorFactory
						//	.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
				if (i == 2)
					marker.position(new LatLng(38.908100,-77.039652)).title("Daniel Anthony")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
						//	.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
				if (i == 3)
					marker.position(new LatLng(38.905779,-77.039073)).title("William Alexander")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				if (i == 4)
					marker.position(new LatLng(38.909252,-77.039287)).title("Alexander Jacob")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
				if (i == 5)
					marker.position(new LatLng(38.906914,-77.040725)).title("Michael Ryan")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
				if (i == 6)
					marker.position(new LatLng(38.904960,-77.044930)).title("Hagrid")
		            .snippet("Available Taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_RED));
				if (i == 7)
					//marker.position(new LatLng(38.907098,-77.036455));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
				if (i == 8)
					//marker.position(new LatLng(38.907098,-77.036455));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
				if (i == 9)
					marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_car_mrk));
					//marker.position(new LatLng(38.907098,-77.036455));
					//marker.icon(BitmapDescriptorFactory
					//		.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

												 
				// Changing marker icon
				
				 
				// adding marker
				//googleMap.addMarker(marker);
				
				
				// Move the camera to last position with a zoom level
				if (i == 1) {
					
					marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.man))
					.title("You")
		            .snippet("Your Location");
					
					//Instantiates a new CircleOptions object +  center/radius
					CircleOptions circleOptions = new CircleOptions()
					.center(new LatLng(38.907983,-77.034094) )
//					.center( new LatLng(randomLocation[0], randomLocation[1]) )
					.radius( 500 )
					.fillColor(0x40ff0000)
					.strokeColor(Color.TRANSPARENT)
					.strokeWidth(2);
					
					// Get back the mutable Circle
					Circle circle = googleMap.addCircle(circleOptions);
					// more operations on the circle...
					
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(new LatLng(38.907983,-77.034094)).zoom(15).build();

					googleMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				}
				googleMap.addPolyline((new PolylineOptions())
						.add(new LatLng(38.907983,-77.034094), new LatLng(38.907098,-77.036455)).width(5).color(Color.BLUE)
						.geodesic(true));
				googleMap.addMarker(marker);
				
		          //googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(38.9047,-77.0164))); 
				
				
				
				//googleMap.addMarker(marker);
			}

			/*//Instantiates a new CircleOptions object +  center/radius
			CircleOptions circleOptions = new CircleOptions()
			.center( new LatLng(fence.getLatitude(), fence.getLongitude()) )
			.radius( fence.getRadius() )
			.fillColor(0x40ff0000)
			.strokeColor(Color.TRANSPARENT)
			.strokeWidth(2);
			
			// Get back the mutable Circle
			Circle circle = mMap.addCircle(circleOptions);
			// more operations on the circle...
*/			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	public void toggleclick(){
		
		if(toggle==0){
		//edt.setBackgroundColor(R.color.bright_foreground_disabled_material_light);
			edt.setBackgroundColor(Color.GRAY);
			edt.setTextColor(Color.WHITE);
		toggle=1; 
		}else{
			edt.setBackgroundColor(Color.TRANSPARENT);
			edt.setTextColor(Color.BLACK);
			toggle=0;
		}
		
	}
	
	/*
	 * creating random postion around a location for testing purpose only
	 */
	/*private double[] createRandLocation(double latitude, double longitude) {

		return new double[] { latitude + ((Math.random() - 0.5) / 500),
				longitude + ((Math.random() - 0.5) / 500),
				150 + ((Math.random() - 0.5) * 10) };
	}*/
}
